﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PROYECTO_FINAL.Models;

namespace PROYECTO_FINAL.Controllers
{
    public class ProductosController : Controller
    {
        private BD_newServirepuestosContainer db = new BD_newServirepuestosContainer();

        // GET: /Productos/
        public ActionResult Index()
        {
            return View(db.ProductosSet.ToList());
        }

        // GET: /Productos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.ProductosSet.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // GET: /Productos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Productos/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id_productos,nombre_productos,descripcion_producto,precio_producto,oferta,fk_categoria")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.ProductosSet.Add(productos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(productos);
        }

        // GET: /Productos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.ProductosSet.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // POST: /Productos/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id_productos,nombre_productos,descripcion_producto,precio_producto,oferta,fk_categoria")] Productos productos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(productos);
        }

        // GET: /Productos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Productos productos = db.ProductosSet.Find(id);
            if (productos == null)
            {
                return HttpNotFound();
            }
            return View(productos);
        }

        // POST: /Productos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Productos productos = db.ProductosSet.Find(id);
            db.ProductosSet.Remove(productos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: /Productos/Repuestos
        public ActionResult Repuestos()
        {
            return View(db.ProductosSet.ToList());
        }

        // GET: /Productos/Lujos
        public ActionResult Lujos()
        {
            return View(db.ProductosSet.ToList());
        }

        // GET: /Productos/Accesorios
        public ActionResult Accesorios()
        {
            return View(db.ProductosSet.ToList());
        }

        // GET: /Productos/Inicio
        public ActionResult Inicio()
        {
            return View(db.ProductosSet.ToList());
        }

        // GET: /Productos/Discos
        public ActionResult Discos()
        {
            return View(db.ProductosSet.ToList());
        }
        
    }


}
