
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/29/2015 19:56:04
-- Generated from EDMX file: C:\Users\Victor\Desktop\PROYECTO_FINAL\PROYECTO_FINAL\PROYECTO_FINAL\Models\BD_newServirepuestos.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [XXX];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ProductosSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ProductosSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ProductosSet'
CREATE TABLE [dbo].[ProductosSet] (
    [id_productos] int IDENTITY(1,1) NOT NULL,
    [nombre_productos] nvarchar(max)  NOT NULL,
    [descripcion_producto] nvarchar(max)  NOT NULL,
    [precio_producto] nvarchar(max)  NOT NULL,
    [oferta] bit  NOT NULL,
    [fk_categoria] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PaginaSet'
CREATE TABLE [dbo].[PaginaSet] (
    [id_pagina] int IDENTITY(1,1) NOT NULL,
    [nombre_pagina] nvarchar(max)  NOT NULL,
    [detalle_pagina] nvarchar(max)  NOT NULL,
    [codigo_pagina] nvarchar(max)  NOT NULL,
    [fk_menu] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'CategoriaSet'
CREATE TABLE [dbo].[CategoriaSet] (
    [id_categoria] int IDENTITY(1,1) NOT NULL,
    [nombre_categoria] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'MenuSet'
CREATE TABLE [dbo].[MenuSet] (
    [id_menu] int IDENTITY(1,1) NOT NULL,
    [nombre_menu] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ImagenesSet'
CREATE TABLE [dbo].[ImagenesSet] (
    [id_imagen] int IDENTITY(1,1) NOT NULL,
    [nombre_imagen] varbinary(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [id_productos] in table 'ProductosSet'
ALTER TABLE [dbo].[ProductosSet]
ADD CONSTRAINT [PK_ProductosSet]
    PRIMARY KEY CLUSTERED ([id_productos] ASC);
GO

-- Creating primary key on [id_pagina] in table 'PaginaSet'
ALTER TABLE [dbo].[PaginaSet]
ADD CONSTRAINT [PK_PaginaSet]
    PRIMARY KEY CLUSTERED ([id_pagina] ASC);
GO

-- Creating primary key on [id_categoria] in table 'CategoriaSet'
ALTER TABLE [dbo].[CategoriaSet]
ADD CONSTRAINT [PK_CategoriaSet]
    PRIMARY KEY CLUSTERED ([id_categoria] ASC);
GO

-- Creating primary key on [id_menu] in table 'MenuSet'
ALTER TABLE [dbo].[MenuSet]
ADD CONSTRAINT [PK_MenuSet]
    PRIMARY KEY CLUSTERED ([id_menu] ASC);
GO

-- Creating primary key on [id_imagen] in table 'ImagenesSet'
ALTER TABLE [dbo].[ImagenesSet]
ADD CONSTRAINT [PK_ImagenesSet]
    PRIMARY KEY CLUSTERED ([id_imagen] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------